package Bank;
import java.util.Arrays;
/**
 * Created by user on 16.10.2016.
 */
public class Bank {

    private Client[] clients = new Client[0];

    private Account[] accounts = new Account[0];

    private Transaction[] transactions = new Transaction[0];

    public Client findClient(Client searchClient) {

        for(Client client : clients) {
            if(client.equals(searchClient)){
                return client;
            }
        }
        return null;
    }

    public Account findAccount (Long searchAccountNumber) {
        for (Account account : accounts) {
            if (account.getAccountNumber()== searchAccountNumber){
                return account ;
            }
        }
        return null;
    }


            public Account [] findAccountsByClient(Client searchClient){
        Account[]clientAccounts = new Account[0];
        for(Account account:accounts){
        if(account.getClient().equals(searchClient)){
        int oldLength = clientAccounts.length;
        clientAccounts = Arrays.copyOf(clientAccounts, oldLength + 1);
        clientAccounts[oldLength] = account;
        }
        }
        return clientAccounts;
        }

    public void addAccount (Client client) {
        Account newAccount = new Account(client, 0.0D);
        int oldLength = clients.length;
        accounts = Arrays.copyOf(accounts, oldLength + 1);
        accounts[oldLength] = newAccount;
    }

        public boolean addClient(Client client){
            if (findClient(client)!= null) {
                System.out.println("The same client  of the bank");
                return false;
            }
            else {
                addAccount(client);
                int oldLength = clients.length;
                clients = Arrays.copyOf(clients, oldLength + 1);
                clients[oldLength] = client;
                return true;
            }

        }
    public boolean putIntoAccount (Long accountNumber, Double amount){
        Account account = findAccount(accountNumber);
        if(account == null){
            System.out.println("Incorrect account number");
            return false;
        }
        else if(amount<=0){
            System.out.println("Amount can't be negative");
            return  false;
        }
        else {
            account.increaseBalance(amount);
             int oldLength = transactions.length;
            transactions = Arrays.copyOf(transactions, oldLength + 1);
            transactions[oldLength] = new Transaction (account, Operation.PUT, amount);
            return true;

        }


    }
    public boolean withdrawFromAccount(Long accountNumber, Double amount) {
        Account account = findAccount(accountNumber);
        if (account == null) {
            System.out.println("Incorrect account number");
            return false;
        } else if (amount <= 0) {
            System.out.println("Amount can't be negative");
            return false;
        } else if (amount > account.getBalance()) {
            System.out.println(" Can't withdraw! Not enough money in the account!");
            return false;

        } else {
            account.decreaseBalance(amount);
            int oldLength = transactions.length;
            transactions = Arrays.copyOf(transactions, oldLength + 1);
            transactions[oldLength] = new Transaction(account, Operation.WITHDRAW, amount);
            return true;
        }
    }


    public boolean moveFromOneAccountToAnother(Long sourceAccountNumber, Long destinationAccountNumber, Double amount) {
        if (withdrawFromAccount(sourceAccountNumber, amount)) {
            if (putIntoAccount(destinationAccountNumber, amount)) {
                return true;
            } else {
                putIntoAccount(sourceAccountNumber, amount);
                return false;
            }
        }
        return false;
    }

    public void printClients() {

        for (Client client : clients) {

            System.out.println(client);
        }

    }

    public void printAccounts() {

        for (Account account : accounts) {

            System.out.println(account);
        }

    }
    public void printAccountsForClient(Client client) {

        System.out.printf(" Accounts Of Clients ", client.getFirstName(), client.getLastName());

        for (Account account : findAccountsByClient(client)) {

            System.out.println(account);

        }

    }
    public void printTransactions(){
        for(Transaction transaction : transactions){
            System.out.println(transaction);
        }

    }
}
