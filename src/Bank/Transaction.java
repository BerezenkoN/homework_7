package Bank;

/**
 * Created by user on 16.10.2016.
 */
public class Transaction {

    private Account source;

    private Operation operation;

    private double amount;

    public Transaction(Account source, Operation operation, double amount) {
        this.operation = operation;
        this.source = source;
        this.amount = amount;
    }

   @Override
    public String toString() {

        return "Transaction:" +
                "source= " + source.getAccountNumber() +
                ", operation= " + operation +
                ", amount= $ " + amount +
                ';';
    }
}