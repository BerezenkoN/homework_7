package Bank;

/**
 * Created by user on 16.10.2016.
 */
public class Account {

    private long accountNumber;

    private Client client;

    private double balance;

    static long GLOBAL_ACCOUNT_NUMBER = 1_000_000L;


    public Account(Client client, double balance) {

        this.client = client;

        this.balance = balance;

        this.accountNumber = ++GLOBAL_ACCOUNT_NUMBER;

    }


    public long getAccountNumber() {

        return accountNumber;

    }

    public void setAccountNumber(long accountNumber) {

        this.accountNumber = accountNumber;

    }


    public Client getClient() {

        return client;

    }


    public double getBalance() {

        return balance;

    }


    public void setBalance(double balance) {

        this.balance = balance;

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Account account = (Account) o;

        if (accountNumber != account.accountNumber) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return (int) (accountNumber ^ (accountNumber >>> 32));
    }

    public void increaseBalance(Double sum) {
        balance += sum;
    }

    public void decreaseBalance(Double sum) {
        balance -= sum;
    }

    @Override
    public String toString() {
        return "Account" +
                "accountNumber=" + accountNumber +
                ", client=" + client +
                ", balance=" + balance +
                '"';
    }

}